CREATE OR REPLACE FUNCTION fnLoadFactSlaughtering() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO cattle."FactSlaughtering"
(
	SELECT
		dd."DateKey" "SlaughteringDateKey"
		,("Bullocks_per_1000_tonnes" * 1000) "BullocksTonnes"
		,("Heifers_per_1000_tonnes" * 1000) "HefiersTonnes"
		,("Cows_per_1000_tonnes" * 1000) "CowsTonnes"
		,("Bulls_per_1000_tonnes" * 1000) "BullsTonnes"
		,("Bullocks_per_1000_Head" * 1000) "BullocksHead"
		,("Heifers_per_1000_Head" * 1000) "HeifersHead"
		,("Cows_per_1000_Head" * 1000) "CowsHead"
		,("Bulls_per_1000_Head" * 1000) "BullsHead"
		,0.001 "PerKg"
		,100 "Per100"
		,1000 "Per1000"
	FROM ods."LivestockSlaughterings" ls
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = ls."Date"
)
ON CONFLICT ("SlaughteringDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactSlaughtering();
END $$;