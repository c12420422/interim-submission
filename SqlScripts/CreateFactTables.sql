CREATE SCHEMA Cattle;
CREATE SCHEMA Economic;
CREATE SCHEMA Dairy;
CREATE SCHEMA Inputs;

DROP TABLE IF EXISTS Cattle."FactCattlePrice";
CREATE TABLE Cattle."FactCattlePrice"
(
	"PriceDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"AnimalTypeKey" INT REFERENCES public."DimAnimalType"("AnimalTypeKey")
	,"250to299KgPerKg" NUMERIC(9,2)
	,"300to349KgPerKg" NUMERIC(9,2)
	,"350to399KgPerKg" NUMERIC(9,2)
	,"400to449KgPerKg" NUMERIC(9,2)
	,"450to499KgPerKg" NUMERIC(9,2)
	,"500to549kgPerkg" NUMERIC(9,2)
	,"Per100Kg" SMALLINT NOT NULL
	,"Per1000Kg" SMALLINT NOT NULL
	,PRIMARY KEY ("PriceDateKey", "AnimalTypeKey")
);

DROP TABLE IF EXISTS Cattle."FactSlaughtering";
CREATE TABLE Cattle."FactSlaughtering"
(
	"SlaughteringDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"BullocksTonnes" INT 
	,"HeifersTonnes" INT
	,"CowsTonnes" INT
	,"BullsTonnes" INT
	,"BullocksHead" INT 
	,"HeifersHead" INT
	,"CowsHead" INT
	,"BullsHead" INT
	,"PerKg" NUMERIC (3, 3)
	,"Per100Tonnes" INT
	,"Per1000Tonnes" INT
	,PRIMARY KEY ("SlaughteringDateKey")
);

DROP TABLE IF EXISTS Economic."FactInflationRate";
CREATE TABLE Economic."FactInflationRate"
(
	"InflationRateDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"AllItems" NUMERIC(4,2)
	,"FoodAndDrink" NUMERIC(4,2)
	,"RestaurantsAndHotels" NUMERIC(4,2)
	,PRIMARY KEY ("InflationRateDateKey")
);

DROP TABLE IF EXISTS Economic."FactConsumerBeefPrice";
CREATE TABLE Economic."FactConsumerBeefPrice"
(
	"ConsumerBeefPriceDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"SirloinSteakKg" NUMERIC(9,2)
	,"StripLoinSteakKg" NUMERIC(9,2)
	,"RoastBeefTopsideKg" NUMERIC(9,2)
	,"DicedBeefKg" NUMERIC(9,2)
	,PRIMARY KEY ("ConsumerBeefPriceDateKey")
);

DROP TABLE IF EXISTS Economic."FactConsumerDairyPrice";
CREATE TABLE Economic."FactConsumerDairyPrice"
(
	"ConsumerDairyPriceDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"MilkLitre" NUMERIC(9,2)
	,"CheeseKg" NUMERIC(9,2)
	,"ButterKg" NUMERIC(9,2)
	,PRIMARY KEY ("ConsumerDairyPriceDateKey")
);

DROP TABLE IF EXISTS Inputs."FactFertiliserPrice";
CREATE TABLE Inputs."FactFertiliserPrice"
(
	"FertiliserPriceDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"CalciumAmmoniumNitrate" NUMERIC(9,2)
	,"Urea" NUMERIC(9,2)
	,"MuriateOfPotash" NUMERIC(9,2)
	,"Compound0_10_20" NUMERIC(9,2)
	,"Compound0_7_30" NUMERIC(9,2)
	,"Compound10_10_20" NUMERIC(9,2)
	,"Compound0_7_14" NUMERIC(9,2)
	,"Compound0_6_12" NUMERIC(9,2)
	,"Compound0_2d5_10" NUMERIC(9,2)
	,PRIMARY KEY ("FertiliserPriceDateKey")
);

DROP TABLE IF EXISTS Inputs."FactFeedPrice";
CREATE TABLE Inputs."FactFeedPrice"
(
	"FeedPriceDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"RolledBarley" NUMERIC(9,2)
	,"SoyaBeanMeal" NUMERIC(9,2)
	,"MaizeMeal" NUMERIC(9,2)
	,"FlakedMaize" NUMERIC(9,2)
	,"CalfNuts16_18PctProtein" NUMERIC(9,2)
	,"CattleNuts13_15PctProtein" NUMERIC(9,2)
	,"CattleMeal13_15PctProtein" NUMERIC(9,2)
	,"DairyNuts" NUMERIC(9,2)
	,"DairyMeal" NUMERIC(9,2)
	,"MilkReplacer" NUMERIC(9,2)
	,"Wheat" NUMERIC(9,2)
	,"EweMeal" NUMERIC(9,2)
	,"EweNuts" NUMERIC(9,2)
	,"LambMeal" NUMERIC(9,2)
	,PRIMARY KEY ("FeedPriceDateKey")
);

DROP TABLE IF EXISTS Dairy."FactDairyPrice";
CREATE TABLE Dairy."FactDairyPrice"
(
	"DairyPriceDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"MilkLitre" NUMERIC(9,2)
	,"MilkLitre3d7PctFat3d3PctProtein" NUMERIC(9,2)
	,PRIMARY KEY ("DairyPriceDateKey")
);

DROP TABLE IF EXISTS Dairy."FactDairySale";
CREATE TABLE Dairy."FactDairySale"
(
	"DairySaleDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"WholeMilk1000Litres" INT
	,"SemiskimmedMilk1000Litres" INT
	,"SkimmedMilk1000Litres" INT
	,"PerLitre" INT
	,PRIMARY KEY ("DairySaleDateKey")
);

DROP TABLE IF EXISTS Dairy."FactDairyIntake";
CREATE TABLE Dairy."FactDairyIntake"
(
	"DairyIntakeDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"CowMilkIntake1000Litres" INT
	,"FatPct" NUMERIC(9,2)
	,"ProteinPct" NUMERIC(9,2)
	,"DomesticIntake1000Litres" INT
	,"ImportIntake1000Litres" INT
	,"PerLitre" INT
	,PRIMARY KEY ("DairyIntakeDateKey")
);

DROP TABLE IF EXISTS Cattle."FactCattleExport";
CREATE TABLE Cattle."FactCattleExport"
(
	"CattleExportDateKey" INT REFERENCES public."DimDate"("DateKey")
	,"CattleHead" INT
	,"BeefTonnes" INT
	,"PerKg" NUMERIC (4, 2)
	,"Per100Tonnes" NUMERIC(9,2)
	,"Per1000Tonnes" NUMERIC(9,2)
	,PRIMARY KEY ("CattleExportDateKey")
);