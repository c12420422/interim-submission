# -*- coding: utf-8 -*-
#!/usr/bin/python3.6
import dash
import dash_core_components as dc
import dash_html_components as dh
from dash.dependencies import Input as dashIn, Output as dashOut
from sqlalchemy import create_engine as ce
import psycopg2 as psy
import dbconfig as dbc
import pandas as pd
import cufflinks as cf

connectString ='postgresql://'+dbc.user+':'+dbc.pwd+'@'+dbc.host+':'+dbc.port+'/'+dbc.db
engine = ce(connectString)

df = pd.read_sql_query('SELECT dd."YearMonth", dd."Year", fcp.*, dat."AnimalTypeName" FROM cattle."FactCattlePrice" fcp INNER JOIN public."DimDate" dd ON dd."DateKey" = fcp."PriceDateKey" INNER JOIN public."DimAnimalType" dat on dat."AnimalTypeKey" = fcp."AnimalTypeKey"', engine)

app = dash.Dash(__name__)

app.title = 'FYP'

app.layout = dh.Div(children=[
    dh.H1(children='Agriculture Visualisation Dashboard', style = dict(marginBottom = '30')),
    dh.Div([
        dh.P('Select one or more weight ranges from the dropdown : '),
        dc.Dropdown(
           id='pricedrop',
           options=[
	       {'label': '250-299Kg', 'value': '250to299KgPerKg'},
	       {'label': '300-349Kg', 'value': '300to349KgPerKg'},
	       {'label': '350-399Kg', 'value': '350to399KgPerKg'},
	       {'label': '400-449Kg', 'value': '400to449KgPerKg'},
               {'label': '450-499Kg', 'value': '450to499KgPerKg'},
               {'label': '500-549Kg', 'value': '500to549kgPerkg'}
           ],
           value = ['250to299KgPerKg'],
           multi = True,
        ),
    ],
    style = dict(
       width = '25%',
       marginLeft = '500'
       ),
    ),
    dh.Div([
        dc.Graph(
            id='CattlePrices',
	    config={
   	        "displaylogo": False,
	        "modeBarButtonsToRemove": ['sendDataToCloud']
	    },
            figure={
                'layout': {
                    'title': 'Cattle Price by Weight Range'
                }
            }
        ),
    ],
    style = dict(
       width = '75%',
       float = 'left',
       marginBottom = '20',
       marginTop = '20'
       ),
    ),
    dh.Div([
        dh.P('Filter by Animal Type : '),
        dc.RadioItems(
            id='animaltypedrop',
            options=[
                {'label': 'Both', 'value': '0'},
                {'label': 'Heifers', 'value': '2'},
                {'label': 'Bullocks', 'value': '1'}
            ], 
            value = '0',
            #multi = False,         
        ),
    ],
    style = dict(
       width = '24%',
       float = 'right',
       marginTop = '200'
       ),
    ),
    dh.Div([
        dh.P('Choose between a monthly and yearly grain : '),
        dc.Dropdown(
            id='yeardrop',
            options=[
                {'label': 'YearMonth', 'value': 'YearMonth'},
                {'label': 'Year', 'value': 'Year'}
            ],
            value = 'YearMonth',
            multi = False,
        ),
    ],
    style = dict(
       width = '25%',
       marginLeft = '500'
       ),
    ),
])

#graphlayout = cf.Layout(height = 500, width = 900)

@app.callback(dashOut('CattlePrices', 'figure'), [dashIn('pricedrop', 'value'), dashIn('yeardrop', 'value'), dashIn('animaltypedrop', 'value')])
def plot_time_series(pricedropval, yeardropval, animalval):
   
    if animalval == '0':
        df2 = pd.DataFrame()
        df2 = pd.concat([df2, df[yeardropval]],axis = 1)
        print(pricedropval, yeardropval)
        for x in pricedropval:
            df2 = pd.concat([df2, df[x]], axis=1)
        df2.set_index(yeardropval, inplace=True)
    else:
        df2 = pd.DataFrame()
        df2 = df.loc[df['AnimalTypeKey'].isin([animalval]),yeardropval]
        df2 = pd.concat([df2, pd.DataFrame(df.loc[df['AnimalTypeKey'].isin([animalval]),pricedropval])] ,axis = 1)
        df2.set_index(yeardropval, inplace=True)
    return df2.groupby([yeardropval]).mean().iplot(asFigure=True, title='Cattle Price Per Kg By Weight Range', yTitle='Price €', xTitle='Date')

server = app.server

if __name__ == '__main__':
    app.run_server(debug=True)
