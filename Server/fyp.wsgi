#! /usr/bin/python

import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/home/projects/fyp/")

from app import server as application
application.secret_key = "secret"
