CREATE OR REPLACE FUNCTION fnLoadFactCattlePrice() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO cattle."FactCattlePrice" AS FCP
(
	SELECT
		dd."DateKey" "PriceDateKey"
		,1 "AnimalTypeKey"
		,ROUND((bullocks_250to299kg_per_100kg / 100)::numeric, 2) "250to299KgPerKg"
		,ROUND((bullocks_300to349kg_per_100kg / 100)::numeric, 2) "300to349KgPerKg"
		,ROUND((bullocks_350to399kg_per_100kg / 100)::numeric, 2) "350to399KgPerKg"
		,ROUND((bullocks_400to449kg_per_100kg / 100)::numeric, 2) "400to349KgPerKg"
		,ROUND((bullocks_450to499kg_per_100kg / 100)::numeric, 2) "450to499KgPerKg"
		,ROUND((bullocks_500to549kg_per_100kg / 100)::numeric, 2) "500to549KgPerKg"
		,100 "Per100Kg"
		,1000 "Per1000Kg"
	FROM ods."CattlePrices" cp
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = cp."Date"
	UNION ALL
	SELECT
		dd."DateKey" "PriceDateKey"
		,2 AnimalTypeKey
		,ROUND((heifers_250to299kg_per_100kg / 100)::numeric, 2) "250to299KgPerKg"
		,ROUND((heifers_300to349kg_per_100kg / 100)::numeric, 2) "300to349KgPerKg"
		,ROUND((heifers_350to399kg_per_100kg / 100)::numeric, 2) "350to399KgPerKg"
		,ROUND((heifers_400to449kg_per_100kg / 100)::numeric, 2) "400to349KgPerKg"
		,NULL
		,NULL
		,100 "Per100Kg"
		,1000 "Per1000Kg"
	FROM ods."CattlePrices" cp
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = cp."Date"
)
ON CONFLICT ("PriceDateKey", "AnimalTypeKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactCattlePrice();
END $$;