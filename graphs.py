# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 17:22:12 2018

@author: Bob
"""
from sqlalchemy import create_engine as ce
import matplotlib as mpl
import pandas as pd

pd.set_option('display.max_rows', None, 'display.max_columns', None)
engine = ce('postgresql://postgres:postgres@localhost:5432/Agriculture')

conDf = pd.read_sql_query('select dd.date as ddate, cp.* from livestockslaughterings cp inner join dim_date dd on dd.date_key = cp.date order by dd.date_key',con=engine)
conDf.fillna(value=pd.np.nan, inplace=True)
conDf.set_index('ddate', inplace=True)
for i in range(1, len(conDf.columns)):
    plt.figure(figsize=(10,5))
    plt.plot(conDf[(conDf.columns[i])])
    plt.ylabel('quantity')
    plt.xlabel('date')
    plt.title(conDf.columns[i])
    mpl.pylab.savefig('livestockslaughterings/' + conDf.columns[i] + '.png', bbox_inches='tight')