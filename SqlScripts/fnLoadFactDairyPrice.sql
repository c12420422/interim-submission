CREATE OR REPLACE FUNCTION fnLoadFactDairyPrice() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO dairy."FactDairyPrice"
(
	SELECT
		dd."DateKey" "DiaryPriceDateKey"
		,"Milk_per_litre_actual_fat_and_protein" "MilkLitre"
		,"Milk_per_litre_3d7_fat_and_3d3_protein" "MilkLitre3d7PctFat3d3PctProtein"
	FROM ods."MilkPrices" ms
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = ms."Date"
)
ON CONFLICT ("DairyPriceDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactDairyPrice();
END $$;