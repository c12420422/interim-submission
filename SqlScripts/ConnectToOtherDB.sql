CREATE EXTENSION postgres_fdw;

CREATE SERVER ODS
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (dbname 'Agriculture');
  
CREATE USER MAPPING FOR CURRENT_USER
SERVER ODS
OPTIONS (user 'bobmccullagh', password 'c12420422');  
  
CREATE SCHEMA ODS;

IMPORT FOREIGN SCHEMA public
FROM SERVER ODS
INTO ODS;

