********InterimSubmission Folder********
--dataquality.csv--
csv file holds the data quality report output from dqreport.py

--Dataset Descriptions.docx--
Contains brief descriptions and links at all datasets downloaded.

--dqreport.py--
This file programatically finds user created tables in the database and creates data quality reports.

--graphs.py--
Deprecated file used to generate graphs of tables for data analysis

--LoadToDB.py--
This file loads all the transformed csv files to tables in the database.

********Server Folder********
This folder contains files found on the ubuntu instance

--app.py--
app.py is the main implementation file for the prototype. It is the flask server hosting the interactive graph.

--dbconfig.py--
simple config file containing username, password, ip etc for connecting to the database instance.

--fyp.wsgi--
user created configuration file for mod_WSGI. Used to interface flask with apache.

--requirements.txt--
User generated file listing all installed packages on the EC2 ubuntu instance.

--assets/style.css--
Basic css used for displaying the prototype.

--fyp.conf--
User created configuration file for the apache web server

__pycache__ is system generated.

********Graphs Folder********
All images in the subfolders are simply matplotlib graphs I used to examine the data.

********RawData Folder********
All files in this folder are the untouched data sources downloaded from the CSO.

********Transformed CSVs Folder********
CSV files found in all four sub folders are the result of mild transformations
made to the raw data before uploading to the database instance.

********SqlScripts Folder********
--ConnectToOtherDB.sql--
Used to create an extension to bring the unformatted tables created in the Agriculture database into the DWH (data warehouse)
database so they can be queried.

--CreateDimAnimalType.SQL--
Script that creates and populates DimAnimalType

--CreateDimDate.SQL--
Script that creates and populates DimDate

--CreateFactTables.SQL--
Create statements for all fact tables so far.

--fnLoadFact*--
The fnLoadFact scripts are used to create functions that populate the fact tables when executed.