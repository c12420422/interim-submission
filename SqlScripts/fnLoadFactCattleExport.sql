CREATE OR REPLACE FUNCTION fnLoadFactCattleExport() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO cattle."FactCattleExport" AS fce
(
	SELECT
		dd."DateKey" "CattleExportDateKey"
		,ROUND(("Exports_of_Cattle_Thousand_Head" * 1000)::numeric, 0) "CattleHead"
		,ROUND(("Exports_of_Beef_Thousand_Tonnes" * 1000)::numeric, 0) "BeefTonnes"
		,0.001 "PerKg"
		,100 "Per100Tonnes"
		,1000 "Per1000Tonnes"
	FROM ods."CattleExports" ce
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = ce."Date"::integer
)
ON CONFLICT ("CattleExportDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactCattleExport();
END $$;