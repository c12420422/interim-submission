DROP TABLE IF EXISTS public."DimDate";
CREATE TABLE public."DimDate" 
(
	"DateKey" INT PRIMARY KEY
	,"DateYYYYMMDD" DATE NOT NULL
	,"DateShortName" VARCHAR(50) NOT NULL
	,"DateLongName" VARCHAR(50) NOT NULL
	,"Month" INT NOT NULL
	,"MonthShortName" VARCHAR(50) NOT NULL
	,"MonthLongName" VARCHAR(50) NOT NULL
	,"Quarter" INT NOT NULL
	,"YearMonth" CHAR(8)  NOT NULL
	,"MonthYear" CHAR(8) NOT NULL
	,"Year" INT NOT NULL
);

INSERT INTO public."DimDate"
SELECT 
	to_char(datum,'yyyymmdd')::INT as DateKey
	,datum as "DateYYYYMMDD"
	,to_char(datum, 'Mon yyyy') as DateShortName
	,to_char(datum, 'Month yyyy') as DateLongName
	,extract(month from datum)::INT as "Month"
	,to_char(datum, 'Mon') as MonthShortName
	,to_char(datum, 'Month') as MonthLongName
	,extract(quarter from datum) as Quarter
	,to_char(datum, 'yyyy-mm') as YearMonth
	,to_char(datum, 'mm-yyyy') MonthYear
	,extract(year from datum)::INT as "Year"
FROM
(
	SELECT '1950-01-01'::DATE + SEQUENCE.DAY AS datum
	FROM generate_series(0,25566) AS SEQUENCE(DAY)
	GROUP BY SEQUENCE.DAY
	ORDER BY datum ASC
) dd
ORDER BY datum;
COMMIT;
