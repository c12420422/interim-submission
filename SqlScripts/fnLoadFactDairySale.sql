CREATE OR REPLACE FUNCTION fnLoadFactDairySale() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO dairy."FactDairySale"
(
	SELECT
		dd."DateKey" "DairySaleDateKey"
		,("Whole_Milk" * 1000) "WholeMilk1000Litres"
		,("Semi-skimmed_milk" * 1000) "SemiskimmedMilk1000Litres"
		,("Skimmed_Milk" * 1000) "SkimmedMilk1000Litres"
		,1000 "PerLitre"
	FROM ods."MilkSales" ms
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = ms."Date"
)
ON CONFLICT ("DairySaleDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactDairySale();
END $$;