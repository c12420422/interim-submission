# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 13:09:18 2018
@author: Bob
"""
import pandas as pd
from sqlalchemy import create_engine as ce
import csv

pd.set_option('display.max_rows', None, 'display.max_columns', None)
engine = ce('postgresql://bobmccullagh:c12420422@mypostgresinstance.c9upktlrlfbq.eu-west-1.rds.amazonaws.com:5432/Agriculture')

result = engine.execute("SELECT table_name FROM information_schema.tables where table_schema not in ('pg_catalog', 'information_schema')")
result = [r[0] for r in result]

for i in range(len(result)):
    conDf = pd.read_sql_query('select * from "' + result[i]+'"',con=engine)
    conDf.fillna(value=pd.np.nan, inplace=True)
    conOut = pd.DataFrame(columns=['Feature','Count','% Miss.','Card','Min','1st Qrt.','Mean', 'Median', '3rd Qrt.', 'Max', 'Std. Dev.'])
    conOut['Feature'] = conDf.columns
    conOut['Count'] = conDf.count().tolist()
    conOut['% Miss.'] = round((conDf.isnull().sum() / (conDf.count() + conDf.isnull().sum())) * 100).tolist()
    conOut['Card'] = conDf.nunique().tolist()
    conOut['Min'] = conDf.min().tolist()
    conOut['1st Qrt.'] = conDf.quantile(0.25).tolist()
    conOut['Mean'] = round(conDf.mean(), 1).tolist()
    conOut['Median'] = conDf.quantile(0.50).tolist()
    conOut['3rd Qrt.'] = conDf.quantile(0.75).tolist()
    conOut['Max'] = conDf.max().tolist()
    conOut['Std. Dev.'] = (round(conDf.std(), 1).tolist())
    print(conOut)
    with open("dataquality.csv", "a", newline = '') as fp:
        wr = csv.writer(fp, dialect='excel')
        wr.writerow([result[i]])
    conOut.to_csv('dataquality.csv', encoding='utf-8', index=False, mode='a')
