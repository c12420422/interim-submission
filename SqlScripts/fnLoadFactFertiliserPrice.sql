CREATE OR REPLACE FUNCTION fnLoadFactFertiliserPrice() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO inputs."FactFertiliserPrice" AS ffp
(
	SELECT
		dd."DateKey" "FertiliserPriceDateKey"
		,"Calcium_Ammonium_Nitrate_27d5_N" "CalciumAmmoniumNitrate"
		,"Urea"
		,"Muriate_of_Potash" "MuriateOfPotash"
		,"Compound_0_10_20"
		,"Compound_0_7_30"
		,"Compound_10_10_20"
		,"Compound_14_7_14"
		,"Compound_18_6_12"
		,"Compound_24_2d5_10"
	FROM ods."FertiliserPrices" fp
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = fp."Date"
)
ON CONFLICT ("FertiliserPriceDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactFertiliserPrice();
END $$;