CREATE OR REPLACE FUNCTION fnLoadFactConsumerBeefPrice() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO economic."FactConsumerBeefPrice"
(
	SELECT
		dd."DateKey" "ConsumerBeefPriceDateKey"
		,"Sirloin_steak_per_kg" "SirloinSteakKg"
		,"Striploin_steak_per_kg" "StriploinSteakKg"
		,"Roast_beef_topside_per_kg" "RoastBeefTopsideKg"
		,"Diced_beef_pieces_per_kg" "DicedBeefKg"
	FROM ods."NationalAveragePrice" nap
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = nap."Date"
)
ON CONFLICT ("ConsumerBeefPriceDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactConsumerBeefPrice();
END $$;
