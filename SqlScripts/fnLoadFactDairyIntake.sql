CREATE OR REPLACE FUNCTION fnLoadFactDairyIntake() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO dairy."FactDairyIntake"
(
	SELECT
		dd."DateKey" "DairyIntakeDateKey"
		,("Milk_Million_Litres" * 1000) "CowMilkIntake1000Litres"
		,("Fat_Content") "FatPct"
		,("Protein_Content") "ProteinPct"
		,("Domestic" * 1000) "DomesticIntake"
		,("Import" * 1000) "ImportIntake"
		,1000 "PerLitre"
	FROM ods."MilkIntake" mi
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = mi."Date"
)
ON CONFLICT ("DairyIntakeDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactDairyIntake();
END $$;
