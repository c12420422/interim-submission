CREATE OR REPLACE FUNCTION fnLoadFactConsumerDairyPrice() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO economic."FactConsumerDairyPrice"
(
	SELECT
		dd."DateKey" "ConsumerDairyPriceDateKey"
		,ROUND("1l_Milk"::numeric, 2) "MilkLitre"
		,ROUND("Irish_cheddar_per_kg"::numeric, 2) "CheeseKg"
		,ROUND(("Butter_per_lb" / 2.205)::numeric, 2) "ButterKg"
	FROM ods."NationalAveragePrice" nap
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = nap."Date"
)
ON CONFLICT ("ConsumerDairyPriceDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactConsumerDairyPrice();
END $$;