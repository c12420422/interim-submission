CREATE OR REPLACE FUNCTION fnLoadFactInflationRate() RETURNS void
LANGUAGE SQL
AS $$
INSERT INTO economic."FactInflationRate"
(
	SELECT
		dd."DateKey" "InflationRateDateKey"
		,"All_Items_One_month_change" "AllItems"
		,"Food_and_Drink_One_month_change" "FoodAndDrink"
		,"Restaurant_and_Hotels_One_month_change" "RestaurantsAndHotels"
	FROM ods."ConsumerPriceIndex" cpi
	INNER JOIN public."DimDate" dd
	ON dd."DateKey" = cpi."Date"
)
ON CONFLICT ("InflationRateDateKey") DO NOTHING;
$$;

DO $$ BEGIN
    PERFORM fnLoadFactInflationRate();
END $$;
