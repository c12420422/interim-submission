# -*- coding: utf-8 -*-
"""
Created on Tue Oct  30 23:13:46 2018

@author: Bob
"""

import pandas as pd
import psycopg2
import sqlalchemy as sa

strengine_dest='postgresql://bobmccullagh:c12420422@mypostgresinstance.c9upktlrlfbq.eu-west-1.rds.amazonaws.com:5432/Agriculture'

# Export Sheep Prices
df = pd.read_csv('Transformed CSVs/Livestock/Sheep Price Per Head and Per kg by Month.csv', sep=',')
df.to_sql('SheepPrice', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Pig Prices
df = pd.read_csv('Transformed CSVs/Livestock/Pig Price Per Head and Per kg by Month.csv', sep=',')
df.to_sql('PigPrice', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Milk Sales
df = pd.read_csv('Transformed CSVs/Livestock/Milk Sales by Type of Milk and Month.csv', sep=',')
df.to_sql('MilkSales', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Manufacturing Milk Prices
df = pd.read_csv('Transformed CSVs/Livestock/Manufacturing Milk and Egg Prices by Product and Month.csv', sep=',')
df.to_sql('MilkPrices', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Livestock Slaughterings
df = pd.read_csv('Transformed CSVs/Livestock/Livestock Slaughterings per 1000 head and 1000 tonnes by Month and Animal Type.csv', sep=',')
df.to_sql('LivestockSlaughterings', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Milk Intake
df = pd.read_csv('Transformed CSVs/Livestock/Intake of Cows Milk by Creameries and Pasteurisers by Domestic or Import Source, and Month.csv', sep=',')
df.to_sql('MilkIntake', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Cattle Exports
df = pd.read_csv('Transformed CSVs/Livestock/Exports of Cattle per 1000 head and Beef per 1000 tonnes by Month.csv', sep=',')
df.to_sql('CattleExports', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Cattle Price
df = pd.read_csv('Transformed CSVs/Livestock/Cattle Price Per Head and Per 100kg by Month.csv', sep=',')
df.to_sql('CattlePrices', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export Fertiliser Price
df = pd.read_csv('Transformed CSVs/Inputs/Fertiliser Price (Euro per Tonne) by Type and Month.csv', sep=',')
df.to_sql('FertiliserPrices', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

# Export CPI
df = pd.read_csv('Transformed CSVs/Economic/Customer Price Index Percentage Change by Month.csv', sep=',')
df.to_sql('ConsumerPriceIndex', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)

#Export National Average Prices
df = pd.read_csv('Transformed CSVs/Economic/National Average Price (Euro) by Month and Item.csv', sep=',')
df.to_sql('NationalAveragePrice', sa.create_engine(strengine_dest), if_exists='replace' ,index = False)
