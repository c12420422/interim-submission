DROP TABLE IF EXISTS public."DimAnimalType";
CREATE TABLE public."DimAnimalType"
(
	"AnimalTypeKey" SERIAL PRIMARY KEY
	,"AnimalTypeID" INT NOT NULL
	,"AnimalTypeName" VARCHAR(50) NOT NULL
);

INSERT INTO "public"."DimAnimalType" ("AnimalTypeID", "AnimalTypeName") VALUES
	(1, 'Bullock')
	,(2, 'Heifer');